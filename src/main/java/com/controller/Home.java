/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.entity.Users;
import com.model.LoginModel;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author naeem
 */
@Controller
public class Home {
    
    @RequestMapping(value="/")
    public String index(Model model)
    {
        //System.out.println("i am from home");
        model.addAttribute("message", "Please Log in the System");
        return "index";
    }
    
    @RequestMapping(value= "/login", method = RequestMethod.POST)
    public String Login(@ModelAttribute(value="Users") Users user,Model model,HttpSession session){
        //System.out.println(user.getUserFullId());
        //model.addAttribute("message",user.getUserFullId()+'#'+user.getPassword());
        LoginModel login= new LoginModel();
        List list = login.getUserByID(user);
        //System.out.println(list);
        
        if(list.size()==0){
            model.addAttribute("userFullId", user.getUserFullId());
             return "index";
        }else
        {
            model.addAttribute("list", list);
            for(int i=0;i<list.size();i++){
                Users u = (Users) list.get(i);
                session.setAttribute("user_name", u.getUserFullId());
                session.setAttribute("user_id", u.getId());
                session.setAttribute("password", u.getPassword());
                
            }
            //System.out.println("session : "+session.getAttribute("user_name"));
            return "user/success";
        }
       
        //return "user/success";
    }
}
