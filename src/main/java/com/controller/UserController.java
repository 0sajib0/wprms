/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.entity.Users;
import com.model.InsertModel;
import com.model.UserModel;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author naeem
 */

@Controller()
@RequestMapping(value="/user")
public class UserController {
    @RequestMapping(value="/page")
    public String index(HttpSession session)
    {
        //System.out.println("user name : " +session.getAttribute("user_name"));
       // System.out.println("user password : " +session.getAttribute("password"));
        return "user/create";
    }
    
    @RequestMapping(value="/insert", method = RequestMethod.POST)
    public String insert(@ModelAttribute(value="Users") Users user,Model m,HttpSession session)
    {
        UserModel insert = new UserModel();
        Date curDate = new Date();
        user.setEDt(new Timestamp(curDate.getTime()));
        user.setEBy((Integer) session.getAttribute("user_id"));
       // System.out.println("User name "+user.getUserFullId()+"\n Password :"+user.getPassword());
        if(insert.userInsert(user)==true){
            m.addAttribute("message","User Creation Successfully");
            return "user/success";
        }else{
           m.addAttribute("message","User Creation error");
            return "user/error"; 
        }
        
    }
    
    @RequestMapping(value ="/users")
    public String userlist(Model m){
        InsertModel view = new InsertModel();
        List list = view.list(); 
         m.addAttribute("elist",list);
        return "user/user_lit";
    }
    
    @RequestMapping(value="/layout")
    public String layout()
    {
        return "layout/index";
    }
    
}
