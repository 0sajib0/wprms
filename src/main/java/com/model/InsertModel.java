/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.entity.Test1;
import com.entity.Users;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author naeem
 */
public class InsertModel {
    SessionFactory factory = HibernateUtil.getSessionFactory();
    Session session = factory.openSession();
    public boolean userInsert(Users user){
        boolean check = false;
        try
        {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            //System.out.println(user.getId()+"###"+user.getName());
            check = true;
        }catch(Exception e){
            session.getTransaction().rollback();
        }
        return check;
        
    }

    public List list() {
        List list = new ArrayList();
        try{
            session.beginTransaction();
            Criteria c = session.createCriteria(Users.class);
            list = c.list();
        }catch(Exception e){
            System.out.println(e);
        }
        
        return list;
    }
    
}
