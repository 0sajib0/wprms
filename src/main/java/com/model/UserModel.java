/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.entity.Users;
import java.util.Date;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author naeem
 */
public class UserModel {
    SessionFactory factory = HibernateUtil.getSessionFactory();
    Session session = factory.openSession();
    
    public boolean userInsert(Users user){
        boolean check = false;
        Date now = new Date();
        user.setSts(1);
        //user.getEDt(new Date())
        //System.out.println("user name:#"+user.getUserFullId()+"#password"+user.getPassword()+"#id"+user.getId()+"#sts:"+user.getSts());
        //System.out.println("edt#",user.getEDt());
        
        try
        {
            session.beginTransaction();
            session.save(user);
            session.getTransaction().commit();
            //System.out.println(user.getId()+"###"+user.getName());
            check = true;
        }catch(Exception e){
            session.getTransaction().rollback();
        }
        return check;
        
    }
    
}
