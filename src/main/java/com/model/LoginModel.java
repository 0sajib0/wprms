/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.model;

import com.entity.Users;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

/**
 *
 * @author naeem
 */
public class LoginModel {
    SessionFactory factory = HibernateUtil.getSessionFactory();
    Session session = factory.openSession();
    public List getUserByID(Users user){
      //List list = new ArrayList();
      //Query q = session.createSQLQuery("select * from Users userFullId = '"+user.getUserFullId()+"'");
      //Query query=session.createQuery("from Users WHERE userFullId = '"+user.getUserFullId()+"'"); 
      Criteria c = session.createCriteria(Users.class);
      c.add(Restrictions.eq("userFullId", user.getUserFullId()));
      List<Users> list = c.list();
      
      return list;
      
    }
    
    public List list() {
        List list = new ArrayList();
        try{
            session.beginTransaction();
            Criteria c = session.createCriteria(Users.class);
            list = c.list();
        }catch(Exception e){
            System.out.print(e);
        }
        
        return list;
    }
    
}
