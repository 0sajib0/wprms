<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <table>
            <c:forEach items="${list}" var="entry">
                <tr>
                    <td>
                    <c:out value="${entry.getUserFullId()}"></c:out>
                        
                    </td> 
                    <td>
                        <c:out value="${entry.getPassword()}"></c:out>
                    </td>
                </tr>
            </c:forEach>
        </table>
        
        <a href="user/page">Create New user</a>
        <a href="user/users">get list</a>
    </body>
</html>
