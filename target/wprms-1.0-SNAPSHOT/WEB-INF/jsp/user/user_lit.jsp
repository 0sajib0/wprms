<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- 
    Document   : user_lit
    Created on : Feb 27, 2019, 12:53:32 AM
    Author     : naeem
--%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table>
            <c:forEach items="${elist}" var="entry">
                <tr>
                    <td>
                        ${entry.userFullId}
                    </td>
                    <td>
                        ${entry.password}
                    </td>    
                </tr>
            </c:forEach>
        </table>
    </body>
</html>
