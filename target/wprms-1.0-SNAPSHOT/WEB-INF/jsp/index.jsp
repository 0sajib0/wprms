<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix ="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link href="<c:url value="/resources/login/login.css"></c:url>" rel="stylesheet">
    <script src="<c:url value="/resources/bootstrap/js/bootstrap.min.js">"</c:url></script>
    <script src="<c:url value="/resources/bootstrap/js/jquery.min.js">"</c:url></script>
    <script src="<c:url value="/resources/bootstrap/js/jquery.min.js">"</c:url> </script>
</head>
<body>
    <div class="wrapper fadeInDown">
      <div id="formContent">
        <!-- Tabs Titles -->

        <!-- Icon -->
        <div class="fadeIn first">
          <img src="<c:url value="/resources/image/logo.png"></c:url>" id="icon" alt="User Icon" />
        </div>

        <!-- Login Form -->
        <form action="login" ModelAttribute="Users" method="post">
          <input type="text" id="login" class="fadeIn second" name="userFullId" placeholder="login" value="${userFullId}">
          <input type="text" id="password" class="fadeIn third" name="password" placeholder="password">
          <input type="submit" class="fadeIn fourth" value="Log In">
        </form>

        <!-- Remind Passowrd -->
        <div id="formFooter">
          <a class="underlineHover" href="#">Forgot Password?</a>
        </div>

      </div>
    </div>
</body>

</html>